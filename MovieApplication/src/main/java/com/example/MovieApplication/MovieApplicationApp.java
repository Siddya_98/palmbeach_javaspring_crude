package com.example.MovieApplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieApplicationApp {

	public static void main(String[] args) {
		SpringApplication.run(MovieApplicationApp.class, args);
		System.err.println("Application Started");
	}

}

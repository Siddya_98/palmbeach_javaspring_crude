package com.example.movie.MovieApplication.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.movie.MovieApplication.model.Booking;
import com.example.movie.MovieApplication.model.Movie;
import com.example.movie.MovieApplication.repository.MovieRepo;

@Service
public class MovieService {
	
	@Autowired
	MovieRepo movieRepo;
	
	
	//Movie movie
	public String saveMovie(Movie movie) {
		
		Movie movie1  = new Movie(); 
		movie1.setMovieId(movie.getMovieId());
		movie1.setMovieName(movie.getMovieName());
		movie1.setGenre(movie.getGenre());
		movie1.setPrice(movie.getPrice());						
		
		Booking bookObj = new Booking();
		
		List<Booking> bookList = movie.getBooking();
		for(int i = 0; i < bookList.size(); i++) {

			bookObj.setBookingId(bookList.get(i).getBookingId());
			bookObj.setBookingName(bookList.get(i).getBookingName());
			bookObj.setBookingStatus(bookList.get(i).getBookingStatus());
			bookObj.setMovie(movie1);
			
			movie1.setBooking(Arrays.asList(bookObj));
			//movie1.setBooking(Arrays.asList(bookObj));

			movieRepo.save(movie1);
		}
		
		
		return "Saved Data";
		
	}
	
	
	public Optional<Movie> getMovie(int movieId) {
	//	movieRepo.getById(movieId);
		Optional<Movie> movie = movieRepo.findById(movieId);
		
		return movie;
		
	}
	
	public List<Movie> getMovieAll() {
	//	movieRepo.getById(movieId);
		List<Movie> allMovies = movieRepo.findAll();
		
		return allMovies;	
	}

	
	public String updateMovie(Movie movie) {
		
		Movie movie1  = new Movie(); 
		movie1.setMovieId(movie.getMovieId());
		movie1.setMovieName(movie.getMovieName());
		movie1.setGenre(movie.getGenre());
		movie1.setPrice(movie.getPrice());						
		
		Booking bookObj = new Booking();
		
		List<Booking> bookList = movie.getBooking();
		for(int i = 0; i < bookList.size(); i++) {

			bookObj.setBookingId(bookList.get(i).getBookingId());
			bookObj.setBookingName(bookList.get(i).getBookingName());
			bookObj.setBookingStatus(bookList.get(i).getBookingStatus());
			bookObj.setMovie(movie1);
			
			movie1.setBooking(Arrays.asList(bookObj));
			//movie1.setBooking(Arrays.asList(bookObj));

			movieRepo.save(movie1);
		}
		
		return "Movie updated";
	}
	
	public String deleteMovie(int movieId) {
		movieRepo.deleteById(movieId);
		return "Data Deleted";
		
	}
	

}

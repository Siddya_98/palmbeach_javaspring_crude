package com.example.movie.MovieApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.movie.MovieApplication.model.Movie;

@Repository
public interface MovieRepo extends JpaRepository<Movie, Integer> {

}

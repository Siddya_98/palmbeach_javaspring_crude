package com.example.movie.MovieApplication.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Movie {
	
	@Id
	private int movieId;
	private String movieName;
	private String genre;
	private double price;
	
	@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
	private List<Booking> booking;
	
	public Movie(int movieId, String movieName, String genre, double price, List<Booking> booking) {
		super();
		this.movieId = movieId;
		this.movieName = movieName;
		this.genre = genre;
		this.price = price;
		this.booking = booking;
	}
	public Movie() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getMovieId() {
		return movieId;
	}
	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public List<Booking> getBooking() {
		return booking;
	}
	public void setBooking(List<Booking> booking) {
		this.booking = booking;
	}
	
	@Override
	public String toString() {
		return "Movie [movieId=" + movieId + ", movieName=" + movieName + ", genre=" + genre + ", price=" + price
				+ ", booking=" + booking + "]";
	}
        
	
	
	
	
}

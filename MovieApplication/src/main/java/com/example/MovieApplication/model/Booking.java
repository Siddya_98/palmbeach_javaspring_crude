package com.example.movie.MovieApplication.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table
public class Booking {

	@Id
	private int bookingId;
	private String bookingName;
	private String bookingStatus;
	@ManyToOne
	@JoinColumn(name = "movie_id")
	@JsonIgnore
	private Movie movie;
	

	public Booking(int bookingId, String bookingName, String bookingStatus, Movie movie) {
		super();
		this.bookingId = bookingId;
		this.bookingName = bookingName;
		this.bookingStatus = bookingStatus;
		this.movie = movie;
	}
	public Booking() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getBookingId() {
		return bookingId;
	}
	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}
	public String getBookingName() {
		return bookingName;
	}
	public void setBookingName(String bookingName) {
		this.bookingName = bookingName;
	}
	public String getBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	@Override
	public String toString() {
		return "Booking [bookingId=" + bookingId + ", bookingName=" + bookingName + ", bookingStatus=" + bookingStatus
				+ ", movie=" + movie + "]";
	}

	
	

}


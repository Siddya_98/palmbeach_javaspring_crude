package com.example.movie.MovieApplication.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movie.MovieApplication.model.Movie;
import com.example.movie.MovieApplication.service.MovieService;

@RestController
public class MovieController {

	@Autowired
	MovieService movieService;

	@PostMapping("/save")
	public ResponseEntity<String> saveMovie(@RequestBody Movie movie) {

		String result = movieService.saveMovie(movie);
		return ResponseEntity.ok(result);
	}
	@GetMapping("/movie/{movieId}")
	public ResponseEntity<Optional<Movie>> getMovieById(@PathVariable("movieId") int movieId) {
		Optional<Movie> movieRecord = movieService.getMovie(movieId);
		return ResponseEntity.ok().body(movieRecord);
	}
	@GetMapping("/movie/all")
	public ResponseEntity<List<Movie>> getMovieAll() {
		List<Movie> movieList = movieService.getMovieAll();
		return ResponseEntity.ok().body(movieList);
	}
	@DeleteMapping("/movie/delete/{movieId}")
	public ResponseEntity<String> movieDeleteById(@PathVariable("movieId") int movieId) {
		String result = movieService.deleteMovie(movieId);
		return ResponseEntity.ok(result);
	}
	@PutMapping("/update/movie")
	public ResponseEntity<String> updateMovie(@RequestBody Movie movie) {
		String result = movieService.updateMovie(movie);
		return ResponseEntity.ok(result);
	}
}
